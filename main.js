import express from "express";
import scoresJSON from "./data/scores.json";

const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("High Scores Server. Visit /scores");
});

app.get("/scores", (req, res) => {
  const sortScores = scoresJSON.sort((a, b) => b.score - a.score);
  const topScores = sortScores.slice(0, 3);
  res.status(200).json(topScores);
});

app.post("/scores", (req, res) => {
  const newScore = req.body;
  scoresJSON.push(newScore);

  res.status(201).send("sent score");
});

app.get("*", (req, res) => {
  res.status(404).json({
    message: "incorrect path",
  });
});

app.listen(port, () => {
  console.log(`Express server on port: ${port}/`);
});
